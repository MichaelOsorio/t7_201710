package test;

import junit.framework.TestCase;
import model.data.structures.RedBlackBST;

public class RedBlackBSTTest extends TestCase
{
	private RedBlackBST<String, Integer> tree;
	
	public void setupEscenario1( )
	{
		tree = new RedBlackBST<String, Integer>();
	}
	
	public void setupEscenario2( )
	{
		setupEscenario1();
		
		tree.put("Gato", 1);
		tree.put("Perro", 2);
		tree.put("Carro", 3);
		tree.put("Casa", 4);
		tree.put("Oso", 5);

	}
	
	public void testAgregar()
	{
		setupEscenario1();
		
		tree.put("Gato", 1);
		tree.put("Perro", 2);
		tree.put("Carro", 3);
		tree.put("Casa", 4);
		tree.put("Oso", 5);
		
		assertFalse( tree.isEmpty() );
		
		assertTrue(tree.contains("Gato"));
		assertTrue(tree.contains("Perro"));
		assertTrue(tree.contains("Carro"));
		assertTrue(tree.contains("Oso"));
		assertTrue(tree.contains("Casa"));
		
	}
	
	public void testDelete()
	{
		setupEscenario2();
		
		tree.delete("Gato");
		assertFalse( tree.contains("Gato"));
		tree.delete("Perro");
		assertFalse( tree.contains("Perro"));
		tree.delete("Carro");
		assertFalse( tree.contains("Carro"));
		tree.delete("Oso");
		assertFalse( tree.contains("Oso"));
		tree.delete("Casa");
		assertFalse( tree.contains("Casa"));
		
		assertTrue( tree.isEmpty() );
	}
	
	public void testDeleteMin()
	{
		setupEscenario2();
		
		tree.deleteMin();
		assertFalse( tree.contains("Carro"));
	}
	
	public void testDeleteMax()
	{
		setupEscenario2();
		
		tree.deleteMax();
		assertFalse( tree.contains("Perro"));
	}

}