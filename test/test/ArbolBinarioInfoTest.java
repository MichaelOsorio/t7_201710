package test;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

import model.data.structures.RedBlackBST;
import model.vo.VOInfoPelicula;

public class ArbolBinarioInfoTest {

	private RedBlackBST<Date, VOInfoPelicula> tree;
	private DateFormat format= new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
	private Date fecha;

	public void setupEscenario1(){
		tree = new RedBlackBST<Date, VOInfoPelicula>();
	}

	public void setupEscenario2(){

		tree = new RedBlackBST<Date, VOInfoPelicula>();



		String[] actores = new String[4]; 
		actores[0] = "Carlos";
		actores[1] = "Luis";
		actores[2] = "Mat Daemon";
		actores[3] = "Leonardo Dicaprio";

		//Se crea el VOPelicula y se le asignan todos los datos
		VOInfoPelicula peli1 = new VOInfoPelicula();
		try{ fecha = format.parse("22 Nov 1995");}catch(Exception e){e.printStackTrace();};
		peli1.setActores(actores);
		peli1.setDirector("Machete");
		peli1.setRatingIMDB(6);
		peli1.setTitulo("Toy Sory");
		peli1.setMovieId(new Long(201548));
		peli1.setFechaLanzamiento(fecha);

		tree.put(peli1.getFechaLanzamiento(), peli1);

	}

	public void setupEscenario3(){

		tree = new RedBlackBST<Date, VOInfoPelicula>();

		//Actores de la peli1
		String[] actores1 = new String[5]; 
		actores1[0] = "Line";
		actores1[1] = "Walt";
		actores1[2] = "Cameron";
		actores1[3] = "Leonardo";
		actores1[4] = "Ana";

		//Se crea el VOPelicula y se le asignan todos los datos
		VOInfoPelicula peli1 = new VOInfoPelicula();

		peli1.setActores(actores1);
		try{ fecha = format.parse("2 Jul 2016");}catch(Exception e){e.printStackTrace();};
		peli1.setDirector("Vin diesel");
		peli1.setMovieId(new Long(6));
		peli1.setRatingIMDB(9);
		peli1.setTitulo("Rain");
		peli1.setFechaLanzamiento(fecha);


		//Se crea la segunda pelicula que ir� en la estructura
		VOInfoPelicula peli2 = new VOInfoPelicula();

		//Actores de la peli2
		String[] actores2 = new String[4]; 
		actores2[0] = "Albert einstein";
		actores2[1] = "Isaac Newton";
		actores2[2] = "Michael Faraday";
		actores2[3] = "Ana Reina";

		try{ fecha = format.parse("3 Dec 1997");}catch(Exception e){e.printStackTrace();};
		peli2.setActores(actores2);
		peli2.setDirector("Marck Zuckemberg");
		peli2.setMovieId(new Long(31416));
		peli2.setRatingIMDB(10);
		peli2.setFechaLanzamiento(fecha);
		
		//Se crea la tercera pelicula que ir� en la estructura
		VOInfoPelicula peli3 = new VOInfoPelicula();

		//Actores de la peli3
		String[] actores3 = new String[6]; 
		actores3[0] = "einstein";
		actores3[1] = "Newton";
		actores3[2] = "Faraday";
		actores3[3] = "Reina";
		actores3[4] = "Bay";
		actores3[5] = "Hitler";
		try{ fecha = format.parse("24 Jan 2020");}catch(Exception e){e.printStackTrace();};
		
		peli3.setActores(actores3);
		peli3.setDirector("Socrates");
		peli3.setMovieId(new Long(98566621));
		peli3.setRatingIMDB(2);
		peli3.setFechaLanzamiento(fecha);

		tree.put(peli1.getFechaLanzamiento(), peli1);
		tree.put(peli2.getFechaLanzamiento(), peli2);
		tree.put(peli3.getFechaLanzamiento(), peli3);
	}

	@Test
	public void testGet(){
		setupEscenario1();

		try{ fecha = format.parse("24 Jan 1000");}catch(Exception e){e.printStackTrace();};
		assertNull("No deber�a retornar un elemento", tree.get(fecha));



		setupEscenario2();

		try{ fecha = format.parse("22 Nov 1995");}catch(Exception e){e.printStackTrace();};
		assertNotNull("Deber�a contener un nodo con el a�o: 1995", tree.get(fecha));


		setupEscenario3();

		try{ fecha = format.parse("2 Jul 2016");}catch(Exception e){e.printStackTrace();};
		assertNotNull("Deber�a contener un nodo con el a�o: 2016", tree.get(fecha));
		
		try{ fecha = format.parse("3 Dec 1997");}catch(Exception e){e.printStackTrace();};
		assertNotNull("Deber�a contener un nodo con el a�o: 1997", tree.get(fecha));
		
		try{ fecha = format.parse("24 Jan 2020");}catch(Exception e){e.printStackTrace();};
		assertNotNull("Deber�a contener un nodo con el a�o: 2020", tree.get(fecha));

		VOInfoPelicula peli1 = new VOInfoPelicula();
		try{ fecha = format.parse("24 Jan 3012");}catch(Exception e){e.printStackTrace();};

		peli1.setFechaLanzamiento(fecha);
		peli1.setRatingIMDB(10);
		peli1.setTitulo("Cars 3");
		peli1.setMovieId(new Long(1547800));

		tree.put(peli1.getFechaLanzamiento(), peli1);

		tree.inOrder();
	}

	@Test
	public void testPut(){
		setupEscenario1();

		try{ fecha = format.parse("24 Jan 3012");}catch(Exception e){e.printStackTrace();};
		assertNull("No deber�a retornar un elemento", tree.get(fecha));

		VOInfoPelicula peli1 = new VOInfoPelicula();

		peli1.setFechaLanzamiento(fecha);
		peli1.setRatingIMDB(10);
		peli1.setTitulo("Cars 3");
		peli1.setMovieId(new Long(1547800));

		tree.put(peli1.getFechaLanzamiento(), peli1);
		assertNotNull("Deber�a existir un elemento con la llave: 3012", tree.get(fecha));

	}
	@Test
	public void testDelete(){
		setupEscenario3();
		
		try{ fecha = format.parse("24 Jan 2020");}catch(Exception e){e.printStackTrace();};
		assertNotNull("Deber�a existir un nodo con la llave = 2020", tree.get(fecha));
		
		tree.delete(fecha);
		
		try{ fecha = format.parse("24 Jan 2020");}catch(Exception e){e.printStackTrace();};
		assertNull("No deber�a existir un nodo con la llave = 2020", tree.get(fecha));
	}


}
