package API;

public interface IRedBlackBST<Key extends Comparable<Key>, Value> 
{
	public int size();
	
	public boolean isEmpty();
	
	public Value get(Key key);
	
	public boolean contains(Key key);
	
	public void put( Key key, Value value );
	
	public void deleteMin();
	
	public void deleteMax();
	
	public void delete(Key key);
	
	public int height( );
	
	public Key min();
	
	public Key max();
	
	public boolean check();
	
	public MyIterator<Key> keys();

}