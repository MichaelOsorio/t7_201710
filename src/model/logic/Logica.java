package model.logic;

import java.util.Date;

import model.data.structures.RedBlackBST;
import model.data.structures.ArbolBinarioInfo;
import model.data.structures.ListaEncadenada;
import model.vo.VOInfoPelicula;

public class Logica {
	
	RedBlackBST<Date, VOInfoPelicula> arbol;
	
	public void cargar(){
		CargadorJson cargador = new CargadorJson();
		
		arbol =new  RedBlackBST<Date, VOInfoPelicula>();
		arbol  = cargador.cargarPeliculas();
	}

	public ListaEncadenada<ArbolBinarioInfo> llavesEnRango(Date lo, Date hi){
		return arbol.llavesEnRango(lo, hi);
	}
	
	public ListaEncadenada<Date> ordenarAgnoMasPeliculas( )
	{
		return arbol.inOrderListaDescendente();
	}
	
	public void peliculasAgno( Date fecha, Long agno )
	{
		ArbolBinarioInfo<Long, VOInfoPelicula> peliculas = arbol.get(fecha);
		ListaEncadenada<Long> listaPeliculas = peliculas.inOrderLista();
		for( int i = 0; i < listaPeliculas.darNumeroElementos(); i++ )
		{
			System.out.println(listaPeliculas.darElemento(i));
		}
	}
}
