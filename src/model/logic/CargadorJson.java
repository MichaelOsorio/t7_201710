package model.logic;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import model.data.structures.RedBlackBST;
import model.vo.VOInfoPelicula;



public class CargadorJson {

	private RedBlackBST<Date, VOInfoPelicula> arbol;

	private VOInfoPelicula pelicula;


	public RedBlackBST<Date, VOInfoPelicula> cargarPeliculas(){

		arbol = new RedBlackBST<Date,VOInfoPelicula>();

		double antes =System.currentTimeMillis();
		JsonParser parser = new JsonParser();
		try
		{
			JsonElement jelement = parser.parse(new FileReader("./data/links_json.json"));
			JsonArray jArray = jelement.getAsJsonArray();



			String rate;
			String titulo;
			String[] actores;
			String actor;
			String director;
			Long movieId;
			double ratingIMDB;
			String date;
			Date fechaLanzamiento = new Date();
			DateFormat format = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);


			JsonPrimitive title;
			JsonPrimitive fecha;
			JsonPrimitive actors;
			JsonPrimitive Director;
			JsonPrimitive rating;




			for(int i = 0; i < jArray.size(); i++)
			{

				JsonObject jobject = jArray.get(i).getAsJsonObject();
				JsonPrimitive id = jobject.getAsJsonPrimitive("movieId");
				movieId = new Long(id.getAsInt());



				JsonObject imdbData = jobject.getAsJsonObject("imdbData");
				title = imdbData.getAsJsonPrimitive("Title");
				titulo =title.getAsString();


				fecha = imdbData.getAsJsonPrimitive("Released");

				date = fecha.getAsString();
				if(!date.equals("N/A"))
				fechaLanzamiento = format.parse(date);

				actors =imdbData.getAsJsonPrimitive("Actors");
				actor = actors.getAsString();
				actores = actor.split(",");

				Director = imdbData.getAsJsonPrimitive("Director");
				director = Director.getAsString();

				rating  = imdbData.getAsJsonPrimitive("imdbRating");
				rate = rating.getAsString();
				if(rate.equals("N/A"))
				{
					ratingIMDB = 0;
				}
				else
					ratingIMDB = Double.parseDouble(rate);



				pelicula = new VOInfoPelicula();
				pelicula.setActores(actores);
				pelicula.setFechaLanzamiento(fechaLanzamiento);
				pelicula.setDirector(director);
				pelicula.setRatingIMDB(ratingIMDB);
				pelicula.setTitulo(titulo);
				pelicula.setMovieId(movieId);

				arbol.put(pelicula.getFechaLanzamiento(), pelicula);

			}

			double despues = System.currentTimeMillis();

			System.out.println("Se cargo la pelicula, la cantidad de peliculas cargadas es: ");
			System.out.println(jArray.size());

			System.out.println("tiempo de ejecucion: " + (despues -antes));


		}
		catch(Exception e)
		{
			e.printStackTrace();

		}
		return arbol;

	}

}
