package Controller;

import java.util.Date;

import model.data.structures.RedBlackBST;
import model.data.structures.ArbolBinarioInfo;
import model.data.structures.ListaEncadenada;
import model.logic.Logica;

public class Controller {
	
	private static Logica logic = new Logica();
	
	public static void cargarDatos(){
		
		logic.cargar();
	}
	
	public static ListaEncadenada<ArbolBinarioInfo> llavesEnRango(Date lo, Date hi){
		return logic.llavesEnRango(lo, hi);
	}
}
