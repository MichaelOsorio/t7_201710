package view;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import com.sun.org.apache.bcel.internal.generic.LLOAD;

import Controller.Controller;
import model.data.structures.RedBlackBST;
import model.data.structures.ArbolBinarioInfo;
import model.data.structures.ListaEncadenada;
import model.vo.VOInfoPelicula;

public class VistaTaller7 {

	private static DateFormat format= new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
	
	public static void main(String[] args) {

		try{
			Controller.cargarDatos();
		}catch (Exception e){
			e.printStackTrace();
		}

		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:
				Date lo = null;
				Date hi =null;
				
				String fecha1;
				String fecha2;
				
				sc=new Scanner(System.in);
				System.out.println("Ingrese la primer fecha con formato d MMMM yyyy: ");
				fecha1 = sc.nextLine();
				sc=new Scanner(System.in);
				System.out.println("Ingrese la segunda fecha con formato d MMMM yyyy: ");
				fecha2 = sc.nextLine();
				
				try{ lo = format.parse(fecha1);}catch(Exception e){e.printStackTrace();};
				try{ hi = format.parse(fecha2);}catch(Exception e){e.printStackTrace();};
				
				if(hi.compareTo(lo) < 1){
					Date temp = lo;
					lo = hi;
					hi =temp;
				}
				
				ListaEncadenada<ArbolBinarioInfo> lista = new ListaEncadenada<ArbolBinarioInfo>();
				lista = Controller.llavesEnRango(lo, hi);
				ArbolBinarioInfo<Long, VOInfoPelicula> tree = new ArbolBinarioInfo<Long, VOInfoPelicula>();

				lista.cambiarActualPrimero();
				for(int i =0; i < lista.darNumeroElementos(); i++){
					tree = lista.darElementoPosicionActual();
					tree.inOrder();
					lista.avanzarSiguientePosicion();
				}
				System.out.println("Los n�meros que se imprimieron corresponden a los id de las pel�culas entre los a�os: " + lo +" y "+ hi);
				
				break;
			case 2:
				break;
			case 3:	
				break;
			case 4: 
				break;
			case 5: 
				break;
			case 6: 
				break;
			case 7:
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:

				break;
			case 11:
				break;
			case 12: 
				fin = true;
				break;

			}


		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("1. pel�culas en rango");
		System.out.println("2. Lista Peliculas por año");
		System.out.println("12. Salir");
	}

}
